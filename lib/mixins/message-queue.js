/* eslint no-undefined: 0 */

'use strict'

const _ = require('lodash')
const debug = require('debug')('loopback:component:mq:mixin')
const rabbit = require('foo-foo-mq');
const loopback = require('loopback')
const errorStackParser = require("error-stack-parser");

const DEFAULT_RESPONSE_MODE = "ack";
const DEFAULT_RESPONSE_ONERROR = "nack";

module.exports = function messageQueueMixin(Model, options) {
  debug('initializing MessageQueue Mixin for model %s with options: %o', Model.modelName, options)

  // Initialize consumers.
  if (options.consumers) {
    Object.keys(options.consumers).forEach(name => {
      const consumer = options.consumers[name]

      debug('initializing consumer %s for model %s. options: %o', name, Model.modelName, consumer)

      const handlerOptions = {
        queue: consumer.queue || '*',
        type: consumer.type || '#',
      }

      rabbit.handle(handlerOptions, msg => {
        debug(`consumer ${name}.${Model.modelName} handling message. exchange: %o, routingKey: %o, type: %o, body: %o`,
          msg.fields.exchange, msg.fields.routingKey, msg.type, msg.body)

        const RabbitMQ = loopback.getModel('RabbitMQ')

        try {
          return Model[name](msg.body, msg)
            .then((res) => {
              debug('consumer %s ran successfully for model %s. key: %o', name, Model.modelName, msg.fields.routingKey);
              let response_mode = consumer.response_mode ? consumer.response_mode : DEFAULT_RESPONSE_MODE;
              let data = {data: res};
              msg[response_mode](data);
            })
            .catch(err => {
              debug(`consumer ${name} failed ${Model.modelName}. key: ${msg.fields.routingKey}: %o`, err)
              let response_onError = consumer.response_onError ? consumer.response_onError : DEFAULT_RESPONSE_ONERROR;
              err = errorStackParser.parse(err);
              let data = {err: JSON.stringify(err)};
              msg[response_onError](data);
            })
        }
        catch (err) {
          debug(`consumer ${name} failed ${Model.modelName}. key: ${msg.fields.routingKey}: %o`, err)
          RabbitMQ.log.error(err)
          let response_onError = consumer.response_onError ? consumer.response_onError : DEFAULT_RESPONSE_ONERROR;
          err = errorStackParser.parse(err);
          let data = {err: JSON.stringify(err)};
          msg[response_onError](data);
        }
      })
    })
  }

  // Initialize producers.
  if (options.producers) {
    Object.keys(options.producers).forEach(name => {
      const producer = Object.assign({ options: { } }, options.producers[name])

      debug(`initializing producer ${name} for model ${Model.modelName}. config: %o`, producer)

      Model[name] = async (body, type, opts) => {
        if (typeof opts === 'undefined' && typeof type === 'object') {
          opts = type
          type = undefined
        }

        body = body || ''
        type = type || undefined
        opts = opts || {}

        const params = _.defaultsDeep({
          body,
          type,
        }, opts, producer.options, { type: 'NOTYPE' })

        debug(`producer ${Model.modelName}.${name} publishing message to exchange ${producer.exchange}: %o`, params)

        let is_request = producer.options.is_request;

        if (is_request){
          let res = await rabbit.request(producer.exchange, params);
          res.ack();
          if (res.body.err) throw new Error(res.body.err);
          return res;
        } else {
          return rabbit.publish(producer.exchange, params);
        }

      }
    })
  }
}
